# Common rules

- Respect people, places & stuff during the event
- help & love each other

To enjoy this Hackathon, **you need Java** on your laptop :

- Hopefully, **you don't need to be a Java developer** to participate & win this Hackathon.
- Enjoy to learn new things, take your time: it's all about fun and discovery !
- Ask if you have any question (Kiet, Jordan, Loic, everyone).
- There is no stupid question, only stuck people afraid to ask !

## Install Java JDK for ubuntu/macOS

- open a terminal
- type `javac -version`, to check if you've a Java installed
- if not: - **for Ubuntu/Debian**
  _ run `sudo apt-get install default-jdk`
  _ open a fresh new terminal and run `javac` - **for macOS :**
  _ use the Ubuntu method OR :
  _ download & install the JDK for macOS : https://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html (file: jdk-10.0.2\*osx-x64_bin.dmg)
  - open a fresh new terminal and run `javac`

## Install Java JDK for Windows

- open a terminal
- type `javac -version`, to check if you've a **Java 10.0.2 or less** installed (so `9` or `1.8.0` is OK too)
- if not:
  _ download & install the JDK for windows : https://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html (file: jdk-10.0.2_windows-x64_bin.exe)
  _ open a fresh new terminal and run `javac`
  _ if it doesn't work
  _ add the JDK bin `C:\Program Files\Java\jdk-10.0.2\bin` in your Path variable environment (@see `./print-screen-window.png` file)

## Clone this project to your laptop

- sign in (or register) on [gitlab](https://gitlab.com)
- clone the hackathon repo on your laptop: `git clone https://gitlab.com/adn-hackathon2018/setup.git`

## Launch RoboCode for Windows

- open explorer in root of your cloned repo (named `setup`), then
- doubleclick `./robocode-1.9.3.3-setup/robocode.bat`

## Launch RoboCode for Ubuntu/Debian/Mac OS

- open terminal in root of your cloned repo (`cd setup`), then
- run `sh ./robocode-1.9.3.3-setup/robocode.sh`

## Create your first robot

### create robot

From top menu :

- Robot -> Source Editor (ctrl + e)
- File -> New -> Robot (ctrl + n)
- Name it & name the packages as you want
- Save your file (ctrl + s) (don't change the path)
- As you can see, your robot now live in `cd robocode-1.9.3.3-setup/robots`

### compile robot

From top menu :

- If it's your very first robot : - Compiler -> Options -> Reset compiler
- Compiler -> Compile (ctrl + b)

## Launch a local training fight

- Back to robocode main frame
- Battle -> New -> add instances of your robots -> Start battle !

# Annexes

## API

- @goto [Robocode API](https://robocode.sourceforge.io/docs/robocode/), choose `Robot` section in `All classes` bottom-left menu

## Afraid by Java ?

#### Very Quick & sufficient introduction (5 minutes)

- [https://learnappdevelopment.com/wp-content/uploads/2016/05/Basic-Java-Cheat-Sheet.pdf](https://learnappdevelopment.com/wp-content/uploads/2016/05/Basic-Java-Cheat-Sheet.pdf)

#### One page cheat sheets :

- [https://www.cheatography.com/sschaub/cheat-sheets/java-fundamentals/pdf_bw/](https://www.cheatography.com/sschaub/cheat-sheets/java-fundamentals/pdf_bw/)

#### Want to do some Math ?

- [http://tutorials.jenkov.com/java/math-operators-and-math-class.html#basic-math-functions](http://tutorials.jenkov.com/java/math-operators-and-math-class.html#basic-math-functions)

#### Multiple files & classes ?

- dont forget to add `package my_package_name` at the beginning of every files of your project, to expose automatically new classes to other files

#### Java in my IDE ?

- In **VSCode**, you can install `Java Extension Pack` extension to make everything easy !
- Other Java Recommended IDE: **Intellij, Eclipse**
- Optional build helper for Linux: You can run `cd robocode-1.9.3.3-setup/robots && sh build.sh myrobot/MyRobot.java` to externally compile your robot, but you have to change compiler option with your own. Ask Loïc, Kiet or Jordan if you want to do that
